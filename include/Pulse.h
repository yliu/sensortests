#include <vector>
#include "TGraph.h"
class Pulse{
  //  const int MAX_SAMPLES = 5000;
 public:
  const char* fileName; 
  std::vector<double> time;
  std::vector<double> volt; 
  double pedHead;
  double pedTail; 
  double ped;
  double rmsHead;
  double rmsTail;
  
  double max;
  double t10;
  double t90;
  double t50;
  double tmax; 
  double slope;
  
  double pulseIntegral;
  int size;
  Pulse();
  ~Pulse();
  Pulse(const char* fname);
  int Print();
  TGraph* gr;
  TGraph* grNormalized; 
  
  
};
