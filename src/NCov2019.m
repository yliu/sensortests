function T=NCov2019()

ncasesCountry = [278;326;547;639;916;2000;2700;4400;6000;7711;9692;11794;14380;17205;20438;24324;28018];
npoints=numel(ncasesCountry);
x = (0:1:npoints-1)';
ncasesHubeiNoWH = [0;67;124;55;157;434;725;1124;1649;2325;3167;3938;4965;6035;7138;8327;9548];
ncasesWuhan= [258;308;320;459;572;618;698;1590;1905;2261;2639;3215;4109;5142;6384;8351;10117];
%ncases = ncasesCountry - ncasesHubeiNoWH - ncasesWuhan;
%ncases=ncasesCountry;
%ncases=ncasesHubeiNoWH;
legends = ["Wuhan","Hubei-Wuhan","China-Hubei","China"];
maxy = [10000;10000;10000;25000];
for jp=1:4
    
    subplot(2,2,jp);
    if(jp==1)
      
       ncases=ncasesWuhan;
    end
    if(jp==2)
       
        ncases =ncasesHubeiNoWH;
    end
    if(jp==3)
       
        ncases =  ncasesCountry - ncasesHubeiNoWH - ncasesWuhan;
    end
    if(jp==4)
       
        ncases = ncasesCountry;
    end
    plot(x,ncases,'o--');
    hold on
    f=fit(x(1:7),ncases(1:7),'exp1');
%plot(f,x,ncases);
%plot(f,x(1:7),ncases(1:7),'o--');
    plot(f);
    f
    g=fit(x(7:npoints),ncases(7:npoints),'poly1');
    plot(g,'--');
    if(jp==1)
        h=fit(x(7:npoints),ncases(7:npoints),'exp1');
        plot(h,'g--');
        h
    end
    g
    xlabel('days since Jan 20, 2020');
    ylabel('# of confirmed cases');
     
     ylim([0, maxy(jp)]);
     legend('off');
     legend(legends(jp));
    hold off
%increaseRate = zeros(npoints,2);
%for jj=1:npoints-1
%   increaseRate(jj,1) = (ncases(jj+1) - ncases(jj))/ncases(jj);
%end
%subplot(2,1,2);
%plot(x(2:npoints), increaseRate(1:npoints-1,1),'o--');
%xlabel('days since Jan 20, 2020');
%ylabel('increase rate of confirmed cases');
end
T=0;
end
