#include "Pulse.h"
#include "TCanvas.h"
#include <fstream>
#include <vector>
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include <iostream>
using namespace std;
int main(int argc, char** argv)
{
  if(argc != 3)
    {
      cout<<"usage :"<<argv[0]<<" input_file.list output.pdf"<<endl; 
      exit(-1);
    }
  gStyle->SetOptStat(1111111);
  gStyle->SetOptFit(11111);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  
  gStyle->SetPalette(1);
  vector<Pulse> pp;
  pp.clear();
  TH1F* hPedHead = new TH1F("pedHead","ped. of head",100, -20, 20.); 
  hPedHead->SetXTitle("ped. of the first 100 samples (mV)");
  
  TH1F* hPedTail = new TH1F("pedTail","ped. of tail",100, -20, 20.);
  hPedTail->SetXTitle("ped. of the last 100 samples (mV)");
  
  TH1F* hPedDiff = new TH1F("hPedDiff","ped tail - head",100, -20, 20);
  hPedDiff->SetXTitle("ped tail - head (mV)");
  
  TH1F* hRMSHead = new TH1F("hRMSHead","RMS of head",100, 0, 50); 
  hRMSHead->SetXTitle("RMS of the first 100 samples (mV)");
  TH1F* hRMSTail = new TH1F("hRMSTail","RMS of tail",100, 0, 50);
  hRMSTail->SetXTitle("RMS of the last 100 samples");
  TH1F* hMax = new TH1F("hMax","max",100, 0., 10.);
  hMax->SetXTitle("pulse height (mV)");
  TH1F* hIntegral = new TH1F("hIntegral","Integral of the pulses (mV*ns)", 100, 0, 200.);
  TH2F* hIntvsMax = new TH2F("hIntVsMax","Integral vs. Max",200, 0., 200., 300, -100., 200.);

  TCanvas* c1 = new TCanvas();
  fstream inputF(argv[1],ios::in);
  string tp;
  while(getline(inputF,tp))
    {
      Pulse p(tp.c_str());
      //  if(p.rmsHead + p.rmsTail > 10.) continue;
      pp.push_back(p);
      hPedHead->Fill(p.pedHead);
      hPedTail->Fill(p.pedTail);
      hPedDiff->Fill(p.pedTail - p.pedHead);
      
      hMax->Fill(p.max);
      hRMSTail->Fill(p.rmsTail);
      hRMSHead->Fill(p.rmsHead);
      hIntegral->Fill(p.pulseIntegral*(p.time[1]-p.time[0]));
      hIntvsMax->Fill(p.max,p.pulseIntegral*(p.time[1]-p.time[0]));
      
    }
  inputF.close();
  c1->Divide(3,3);
  c1->cd(1); hPedHead->Draw();
  c1->cd(2); hPedTail->Draw();
  c1->cd(3); hRMSHead->Draw();
  c1->cd(4); hRMSTail->Draw();
  c1->cd(5); hMax->Fit("landau");hMax->Draw();gPad->SetLogy();
  c1->cd(6); hPedDiff->Draw();
  c1->cd(7); hIntegral->Draw();
  c1->cd(8);hIntvsMax->Draw("COLZ");
  c1->Print(argv[2]);
  
  return 0;
}
