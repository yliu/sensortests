#include <fstream>
#include <sstream>
#include <string>
#include <vector> 
#include <algorithm>
#include "Pulse.h"
#include <iostream>
#include "math.h"
using namespace std;
#include "TGraph.h"

TGraph* makeGraph(vector<double> x, vector<double> y)
{
  if(x.size() != y.size())
    {
      cout<<"x,y mismatch in sizes"<<endl; 
      return NULL;
    }
  const static int n = x.size();

  double gy[10000]={0};
  double gx[10000]={0};
  for(int i=0; i!=n; ++i) 
    {
      gx[i] = x[i];
      gy[i] = y[i];
      
    }
  TGraph* gr = new TGraph(n,gx,gy);
  return gr;
}

double average(vector<double> s, int beg, int end)
{
  double sum = 0.;
  for(int i=beg; i!=end+1; ++i)
    {
      sum += s[i];
    }
  return sum/(end-beg+1);
}
double stderror(vector<double> s, int beg, int end)
{
  double s0 = average(s,beg,end);
  double sum2 = 0.;
  for(int i=beg; i!=end+1;++i)
    {
      sum2 += pow(s[i]-s0, 2.);
    }
  sum2 /= end - beg;
  return sqrt(sum2);
  
}
Pulse::Pulse()
{
  gr=0;
  // do nothing;
}

Pulse::~Pulse()
{
  time.clear();
  volt.clear();
  // if(gr) 
  // delete gr;
  
}
Pulse::Pulse(const char* fname)
{
  fileName = fname; 
  time.clear();
  volt.clear();
  fstream inputF;
  inputF.open(fileName,ios::in);
  int lineNumber = 0; 
  string tp;
  grNormalized = 0; 
  while(getline(inputF, tp))
    {
      ++lineNumber;
      if(lineNumber < 6) continue;// 5 lines of header 
      stringstream ss(tp);
      double t,v;
      char c;
      ss>>t>>c>>v;
      time.push_back(t*1e+9); // convert to ns
      volt.push_back(v*1e3); // convert to mV
      
    }
  size = volt.size();
  inputF.close();
  max = *max_element(volt.begin(),volt.end());
  pedHead = average(volt,0,99);
  pedTail = average(volt,size-100, size-1);
  ped = pedHead;
  for(int i=0;i!=size; ++i)
    {
      volt[i] -= ped;
    }
  vector<double>::iterator ItMax = max_element(volt.begin(), volt.end());
  int iMax = std::distance(volt.begin(),ItMax);
  
  
  max = *ItMax;
  tmax = time[iMax];
  for(int i=iMax; i!=0; --i)
    {
      if( (volt[i]-0.9*max)*(volt[i-1]-0.9*max) < 0.)
	t90 = time[i];
      if( (volt[i]-0.1*max)*(volt[i-1]-0.1*max) < 0.)
	t10 = time[i];
      if( (volt[i]-0.5*max)*(volt[i-1]-0.5*max) < 0.)
	t50 = time[i];
    }
  double nx[201] = {0};
  double ny[201] = {0};
  int j=0;
  if(iMax > 100){
    for(int i=iMax - 100; i!= iMax + 101; ++i)
      {
	nx[j] = time[i] - tmax; 
	ny[j] = volt[i]/max; 
	++j;
      }
    grNormalized = new TGraph(201, nx, ny);
  }
  slope = 0.8*max/(t90-t10);
  
  pedHead = average(volt,0,99);
  pedTail = average(volt,size-100, size-1);
  ped = (pedHead + pedTail)/2.;
  
  rmsHead = stderror(volt,0,99);
  rmsTail = stderror(volt,size-100,size-1);
  pulseIntegral = average(volt,0,size-1)*size; 
  gr = makeGraph(time,volt);
  
  
  

}
int Pulse::Print()
{
  for(int i=0; i!=size;++i)
    {
      cout<<" == "<<time[i]<<"/"<<volt[i]<<endl;
    }
  cout<<"max = "<<max<<"\n"
      <<"pedHead = "<<pedHead<<"  pedTail ="<<pedTail<<"\n"
      <<"rmsHead = "<<rmsHead<<"  rmsTail = "<<rmsTail<<"\n"
      <<"Integral = "<<pulseIntegral<<endl;
  
  return 0;
}
