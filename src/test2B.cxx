#include "Pulse.h"
#include "TCanvas.h"
#include <fstream>
#include <vector>
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TFile.h"
#include <iostream>
#include "TTree.h"
using namespace std;
int main(int argc, char** argv)
{
  if(argc != 4)
    {
      cout<<"usage :"<<argv[0]<<"trigger.list DUT.list  output.pdf"<<endl; 
      exit(-1);
    }
  // argv[1] = trigger_osc data file.list
  // argv[2] = DUT_osc data file.list 
  // argv[3] = output canvas PDF
  
  gStyle->SetOptStat(1111111);
  gStyle->SetOptFit(11111);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  
  gStyle->SetPalette(1);
  vector<Pulse> pp; //Trigger pulses
  vector<Pulse> dp; //DUT pulses 
  pp.clear();
  dp.clear();
  TH2F* h2 = new TH2F("h2","h2",2,-4,4,2,-.2, 1);
  vector<TH1*> allHists;allHists.clear();
  TFile* fresult = new TFile(argv[3],"CREATE");
  TH1F* hPedHead = new TH1F("pedHead","ped. of head",100, -20, 20.); 
  hPedHead->SetXTitle("ped. of the first 100 samples (mV)");
  allHists.push_back(hPedHead);
  TH1F* hPedTail = new TH1F("pedTail","ped. of tail",100, -20, 20.);
  hPedTail->SetXTitle("ped. of the last 100 samples (mV)");
  allHists.push_back(hPedTail);
  TH1F* hPedDiff = new TH1F("hPedDiff","ped tail - head",100, -20, 20);
  hPedDiff->SetXTitle("ped tail - head (mV)");
  allHists.push_back(hPedDiff);
  TH1F* hRMSHead = new TH1F("hRMSHead","RMS of head",100, 0, 2); 
  hRMSHead->SetXTitle("RMS of the first 100 samples (mV)");
  allHists.push_back(hRMSHead);
  TH1F* hRMSTail = new TH1F("hRMSTail","RMS of tail",100, 0, 2);
  hRMSTail->SetXTitle("RMS of the last 100 samples");
  allHists.push_back(hRMSTail);
  TH1F* hMax = new TH1F("hMax","max",100, 0., 50.);
  hMax->SetXTitle("pulse height (mV)");
  allHists.push_back(hMax);
  TH1F* hIntegral = new TH1F("hIntegral","Integral of the pulses (mV*ns)", 100, 0, 200.);
  allHists.push_back(hIntegral);
  TH2F* hIntvsMax = new TH2F("hIntVsMax","Integral vs. Max",200, 0., 200., 300, -100., 200.);
  allHists.push_back(hIntvsMax);
  TH1F* hDUTslope = new TH1F("slope","slope",100, 0., 10.);
  allHists.push_back(hDUTslope);
  TH1F* hDUTt10 = new TH1F("t10","t10",100,-10, 10);
  allHists.push_back(hDUTt10);
  TH1F* hDUTt90 = new TH1F("t90","t90",100, -10, 10);
  allHists.push_back(hDUTt90);
  TH1F* hDUT_t50rel = new TH1F("t50rel","t50rel",100, 0,2.);
  allHists.push_back(hDUT_t50rel);

  // write the following variables to a tree:
  // DUT_t10, DUT_t90, DUT_Vmax, DUT_RMS
  // Trig_t10, Trig_t90, Trig_Vmax, Trig_RMS
  TTree data("timingTree","DUT and Trig signal properties");
  double DUT_t10, DUT_t90, DUT_t50, DUT_tmax, DUT_Vmax, DUT_RMS;
  double  Trig_t10, Trig_t90, Trig_t50, Trig_Vmax,Trig_tmax, Trig_RMS;
  data.Branch("DUT_t10",&DUT_t10,"DUT_t10/D");
  data.Branch("DUT_t90",&DUT_t90,"DUT_t90/D");
  data.Branch("DUT_t50",&DUT_t50,"DUT_t50/D");
  data.Branch("DUT_tmax",&DUT_tmax,"DUT_tmax/D");
  data.Branch("DUT_Vmax",&DUT_Vmax,"DUT_Vmax/D");
  data.Branch("DUT_RMS",&DUT_RMS,"DUT_RMS/D");
  
  data.Branch("Trig_t10",&Trig_t10,"Trig_t10/D");
  data.Branch("Trig_t90",&Trig_t90,"Trig_t90/D");
  data.Branch("Trig_tmax",&Trig_tmax,"Trig_tmax/D");
  data.Branch("Trig_t50",&Trig_t50,"Trig_t50/D");
  data.Branch("Trig_Vmax",&Trig_Vmax,"Trig_Vmax/D");
  data.Branch("Trig_RMS",&Trig_RMS,"Trig_RMS/D");
  

  


  TCanvas* c1 = new TCanvas();
  fstream triggerData(argv[1],ios::in);
  string tp;
  while(getline(triggerData,tp))
    {
      Pulse p(tp.c_str());
      //  if(p.rmsHead + p.rmsTail > 10.) continue;
      pp.push_back(p);
     
      
    }

  triggerData.close();
  cout<<"numbers of pulses in Trigger file = "<<pp.size()<<endl; 
  fstream dutData(argv[2],ios::in);
  while(getline(dutData,tp))
    {
      Pulse p(tp.c_str());
      dp.push_back(p);
      hPedHead->Fill(p.pedHead);
      hPedTail->Fill(p.pedTail);
      hPedDiff->Fill(p.pedTail - p.pedHead);
      

      hRMSTail->Fill(p.rmsTail);
      hRMSHead->Fill(p.rmsHead);
      if(p.max > 10.*p.rmsHead){
	hMax->Fill(p.max);
	hDUTt10->Fill(p.t10);
	hDUTt90->Fill(p.t90);
	hDUTslope->Fill(p.slope);
	
      }
      hIntegral->Fill(p.pulseIntegral*(p.time[1]-p.time[0]));
      hIntvsMax->Fill(p.max,p.pulseIntegral*(p.time[1]-p.time[0]));
      
    }
  int npulses = dp.size();
  cout<<"numbers of pulses in DUT file = "<<npulses<<endl;      
  dutData.close();
  int npass=0;
  int ipick = 0; 
  double ax[201] = {0};
  double ay[201]= {0};
  for(int i=0; i!=npulses; ++i)
    {
      DUT_t10 = dp[i].t10;
      DUT_t50 = dp[i].t50;
      DUT_t90 = dp[i].t90;
      DUT_tmax = dp[i].tmax;
      DUT_Vmax = dp[i].max;
      DUT_RMS = dp[i].rmsHead;
      Trig_t10 = pp[i].t10;
      Trig_t50 = pp[i].t50;
      Trig_t90 = pp[i].t90;
      Trig_Vmax = pp[i].max;
      Trig_RMS = pp[i].rmsHead;
      Trig_tmax = pp[i].tmax;
      data.Fill();
      c1->cd();

      if(DUT_Vmax > DUT_RMS*8.&& Trig_Vmax > Trig_RMS*10&&fabs(DUT_Vmax - 5.5)<0.5)
	{
	  ++npass;
	  c1->cd();
	  hDUT_t50rel->Fill(dp[i].tmax - dp[i].t50);
	  for(int ipoint = 0; ipoint != 201; ++ipoint)
	    {
	      ax[ipoint] += dp[i].grNormalized->GetPointX(ipoint);
	      ay[ipoint] += dp[i].grNormalized->GetPointY(ipoint);
	      
	    }
	  if(npass == 1){
	    h2->Draw();
	    (dp[i].grNormalized)->Draw("P");
	    
	    ipick = i;
	  }
	  else
	    (dp[i].grNormalized)->Draw("P");
	}

    }
  for(int ipoint=0; ipoint!=201; ++ipoint)     
    {
      ax[ipoint] /= npass;
      ay[ipoint] /= npass;
    }
  TGraph* gav = new TGraph(201,ax,ay);
  gav->SetLineColor(kRed);
  gav->Draw("LC");
  gav->Fit("pol1","","",-0.6, -0.2);
  
  fresult->cd();
  for(vector<TH1*>::iterator ih = allHists.begin(); ih!=allHists.end(); ++ih)
    (*ih)->Write();
  data.Write();
  dp[ipick].gr->Write("pulse");
  fresult->Close();
  c1->Print("pulse.pdf");
  return 0;
}
